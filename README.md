Tool to list volume on a path in LHCb
======================================


Building
--------
```
make
```

Requires the LHCb environment script and a CMTCONFIG compatible with Brunel v54r1
   
   
Running
-------
```
./BrunelDev_v54r1/run python checkSensorsCrossed.py 
```

Change the script to modify the DDDB tag and the paths traversed.
