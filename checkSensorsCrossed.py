#!/usr/bin/env python

from Gaudi.Configuration  import *
from Configurables import CondDB, DDDBConf
import GaudiPython
from GaudiPython.Bindings import gbl, AppMgr, Helper
from Configurables import TransportSvc, ApplicationMgr

import json
import ROOT
from ROOT import TVector3, TCanvas, TH2D
from ROOT.Math import Pi
from ROOT import TMath
import numpy as np
import array
import sys
import argparse

# #############################################################################
# DDDB Configuration
# #############################################################################
d = DDDBConf()
d.DataType="Upgrade"
d.Simulation = True
#CondDB().Tags["DDDB"]="master"
#CondDB().Tags["SIMCOND"]=""

# #############################################################################
# gaudiPython configuration
# #############################################################################

# Setting up the main application
ApplicationMgr( OutputLevel = INFO, AppName = 'matmap', ExtSvc = ['TransportSvc'])

# Setting up the classes to interact with the transport service from python
std = gbl.std
#Interval=std.pair('double','double')
#Intersection = std.pair(Interval,'const Material*')
Intersection = std.IntersectionHolder
Intersections= std.vector(Intersection)
Point  = gbl.ROOT.Math.XYZPoint
Vector = gbl.ROOT.Math.XYZVector
LHCb = gbl.LHCb

# Looking up the services
appMgr = GaudiPython.AppMgr(outputlevel=4)
evtSvc = appMgr.evtSvc()
det = appMgr.detsvc()
tranSvc = appMgr.service('TransportSvc','ITransportSvc')
if tranSvc is None:
    print("Could not locate the transport service, exiting")
    sys.exit(1)
tranSvc.OutputLevel = DEBUG

# starting
appMgr.initialize()
appMgr.start()

# #############################################################################
# Geometrical tools
# #############################################################################

def getIntersectionsThetaRad(Theta_rad, Phi_rad, IP):
    CosTheta=TMath.Cos(Theta_rad)
    SinTheta=TMath.Sin(Theta_rad)
    CosPhi=TMath.Cos(Phi_rad)
    SinPhi=TMath.Sin(Phi_rad)
    intersept = Intersections()
    Dir = Vector(SinTheta*CosPhi, SinTheta*SinPhi, CosTheta)    # 3-D test-particle (unit) direction vector
    Walls = tranSvc.intersections( IP, Dir, 0.0, 2000.0, intersept, 0.0001) # transport test-particle from IP, towards Dir, for 2000.0 mm
    return intersept

def getIntersectionsZEtaPhi(z, eta, phi):
    ''' Find the numbers of sensors encountered on a segment starting at (0, 0, z)
    in the direction (eta, phi) '''
    o = Point(0.0, 0.0, z)
    theta = 2 * TMath.ATan(TMath.Exp(-eta))
    direction = Vector(TMath.Sin(theta) * TMath.Cos(phi),
                       TMath.Sin(theta) * TMath.Sin(phi),
                       TMath.Cos(theta))
    r =  20000.0
    intersept = Intersections()
    Walls = tranSvc.intersections( o, direction, 0.0, r, intersept, 0.0001) 
    return intersept

def getIntersections(p1, p2):

    direction = p2 - p1
    intersept = Intersections()
    Walls = tranSvc.intersections( p1, direction / direction.R(), 0.0, direction.R(), intersept, 0.0001) 
    return intersept

def show(i):
    print("\t".join([str(i.first.first), str(i.first.second), i.second.name(), i.path]))
  

# #############################################################################
# Now scanning the geometry 
# #############################################################################

parser = argparse.ArgumentParser()
parser.add_argument("--z0", help="set origin",
                    action="store",  type=float, default=0.0)
parser.add_argument('eta', type=float)
parser.add_argument('phi', type=float)
args = parser.parse_args()

intersepts = getIntersectionsZEtaPhi(args.z0, args.eta, args.phi)
print "intercepts nb:", len(intersepts)
for i in intersepts:
    show(i)

# p1=Poingt(args.x0, args.y0, args.z0)
# p2=Point(200, 200, 10000)
# intersepts = getIntersections(p1, p2)
# print "intercepts nb:", len(intersepts)
# for i in intersepts:
#     print("\t".join([str(i.first.first), str(i.first.second), i.second.name(), i.path]))

            
