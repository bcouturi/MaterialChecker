



DEVSUBDIR=./BrunelDev_v54r1


all: build

setup.done:
	lb-dev Brunel/v54r1;
	cd $(DEVSUBDIR) && git lb-use LHCb 
	cd $(DEVSUBDIR) && git lb-checkout LHCb/IntersectionHolder Det/DetDesc
	cd $(DEVSUBDIR) && git lb-checkout LHCb/IntersectionHolder Det/DetDescSvc
	touch setup.done


build: setup.done
	$(MAKE) -C $(DEVSUBDIR) 
